# To jest miejsce od którego należy zacząć zadanie TDD
import pytest

from gaderypoluki.caesar import Caesar


@pytest.mark.parametrize("shift, test_input, expected", [
    (1, "wąż", "yba"),
    (33, "wąż", "yba"),
    (65, "wąż", "yba"),
    (97, "wąż", "yba"),
    (31, "żaba", "źżąż"),
    (0, "kot nie ma ali", "kot nie ma ali"),
    (32, "ala ma kota", "ala ma kota"),
    (64, "ala ma kota", "ala ma kota"),
    (96, "ala ma kota", "ala ma kota"),
    (7, "KAT", "KAT"),
    (3, "wĄŻ ŻaBa", "źĄŻ ŻcBc")
])
def test_caesar_translate(shift, test_input, expected):
    translator = Caesar(shift)
    assert translator.translate(test_input) == expected


@pytest.mark.parametrize("shift", [
    1,
    31,
    0,
    32,
])
def test_should_check_code_length(shift):
    translator = Caesar(shift)
    size = translator.get_code_length()

    assert size == 32


def test_should_check_translatable():
    # given
    shift = 5
    translator = Caesar(shift)
    c = "g"

    # when
    result = translator.is_translatable(c)

    # then
    assert result


def test_should_check_not_translatable():
    # given
    shift = 5
    translator = Caesar(shift)
    c = "Z"

    # when
    result = translator.is_translatable(c)

    # then
    assert not result


def test_should_translate_ignore():
    # given
    shift = 3
    translator = Caesar(shift)
    msg = "MĘŻNY"

    # when
    result = translator.translate_ignore_case(msg)

    # then
    assert result == "ohbóż"


def test_should_throw_exception():
    # given
    shift = 3
    translator = Caesar(shift)

    # when
    with pytest.raises(Exception):
        translator.translate(None)

    # then
