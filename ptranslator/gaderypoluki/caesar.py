from gaderypoluki.cipher_interface import Cipher


class Caesar(Cipher):
    aplhabet = [
        'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó',
        'p',
        'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż']
    lenght = len(aplhabet)

    # Konstruktor
    def __init__(self, shift):
        self.map = dict()
        lenght = len(self.aplhabet)

        for num, letter in enumerate(self.aplhabet):
            i = (num + shift) % lenght  # czyścimy "pełne" przesunięcia
            self.map[letter] = self.aplhabet[i]
