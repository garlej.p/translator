class Cipher(object):

    # Główna metoda translacji
    def translate(self, msg):
        result = ""
        if msg is None:
            raise Exception('Null msg not allowed')
        for c in msg:
            if c in self.map.keys():
                result += self.map[c]
            else:
                result += c

        return result

    # dodatkowa metoda translacji, ignorująca wielkość znaków
    def translate_ignore_case(self, msg):
        return self.translate(msg.lower())

    # sprawdza czy znak zostanie przetłumaczony dla danego klucza
    def is_translatable(self, c):
        return c in self.map.keys()

    # pobiera aktualną długość klucza szyfrującego
    def get_code_length(self):
        return len(self.map.keys())
