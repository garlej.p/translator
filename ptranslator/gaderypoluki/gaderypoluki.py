# Translator podstawieniowy na bazie klucza 'gaderypoluki'
# pl.wikipedia.org/wiki/Gaderypoluki
from gaderypoluki.cipher_interface import Cipher


class GaDeRyPoLuKi(Cipher):

    # Konstruktor
    def __init__(self):
        self.map = {"g": "a", "a": "g", "d": "e", "e": "d", "r": "y", "y": "r", "p": "o", "o": "p", "l": "u", "u": "l",
                    "k": "i", "i": "k"}
